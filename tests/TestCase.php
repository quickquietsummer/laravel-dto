<?php

namespace Qqs\Dto\Tests;

use Qqs\Dto\Providers\ConsoleServiceProvider;
use Qqs\Dto\Providers\DtoBaseServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app): array
    {
        return [
            DtoBaseServiceProvider::class,
            ConsoleServiceProvider::class
        ];
    }
}