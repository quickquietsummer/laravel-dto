<?php

namespace Qqs\Dto\Tests\Feature;

use Qqs\Dto\Tests\TestCase;

class ConsoleTest extends TestCase
{
    public function test_make_command_available()
    {
        $this->artisan('list')
            ->expectsOutputToContain('make:dto');
    }
}