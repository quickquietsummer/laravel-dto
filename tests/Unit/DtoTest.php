<?php

namespace Qqs\Dto\Tests\Unit;

use Carbon\Carbon;
use InvalidArgumentException;
use Qqs\Dto\Tests\TestCase;
use Qqs\Dto\Tests\Unit\Mocks\GoodDto;
use Qqs\Dto\Tests\Unit\Mocks\GoodDtoWithNarrowedType;
use Qqs\Dto\Tests\Unit\Mocks\SomeChild;
use Qqs\Dto\Tests\Unit\Mocks\SomeParent;
use Qqs\Dto\Tests\Unit\Mocks\UntypedDto;
use RuntimeException;

class DtoTest extends TestCase
{
    public function test_dto_fill_properties()
    {
        $data = [
            'name' => 'ALAHAHAHA',
            'password' => '12345678',
            'email' => 'stewt@mail.ru',
            'birthdate' => Carbon::now(),
            'age' => 20,
            'parent' => new SomeParent('some')
        ];

        $registerDTO = new GoodDto($data);

        $this->assertEquals($data['name'], $registerDTO->name);
        $this->assertEquals($data['password'], $registerDTO->password);
        $this->assertEquals($data['email'], $registerDTO->email);
    }

    public function test_ok_if_narrowed_type_passed()
    {
        $data = [
            'name' => 'ALAHAHAHA',
            'password' => '12345678',
            'email' => 'stewt@mail.ru',
            'birthdate' => Carbon::now(),
            'age' => 20,
            'parent' => new SomeChild('some')
        ];
        // В дто требуется базовый родительский класс. Суженный тип разрешен

        $registerDTO = new GoodDto($data);

        $this->assertEquals($data['name'], $registerDTO->name);
        $this->assertEquals($data['password'], $registerDTO->password);
        $this->assertEquals($data['email'], $registerDTO->email);
    }

    public function test_throw_exception_if_widened_class_passed()
    {
        $this->expectException(InvalidArgumentException::class);
        $data = [
            'name' => 'ALAHAHAHA',
            'password' => '12345678',
            'email' => 'stewt@mail.ru',
            'birthdate' => Carbon::now(),
            'age' => 20,
            'parent' => new SomeParent('some')
        ];
        // В дто требуется специфичный дочерний класс. Базовый тип запрещен

        new GoodDtoWithNarrowedType($data);
    }

    public function test_dto_throw_exception_if_missing_required_data()
    {
        $this->expectException(InvalidArgumentException::class);
        $data = [
            'name' => 'ALAHAHAHA',
            'email' => 'stewt@mail.ru'
        ];
        new GoodDto($data);
    }

    public function test_dto_throw_exception_if_untyped()
    {
        $this->expectException(RuntimeException::class);
        $data = [
            'name' => 'ALAHAHAHA',
            'email' => 'stewt@mail.ru'
        ];
        new UntypedDto($data);
    }

    public function test_dto_throw_exception_if_wrong_type()
    {
        $this->expectException(InvalidArgumentException::class);
        $data = [
            'name' => 'ALAHAHAHA',
            'password' => '12345678',
            'email' => 'stewt@mail.ru',
            'age' => '64',
            'birthdate' => Carbon::now(),
        ];
        new GoodDto($data);
    }

}
