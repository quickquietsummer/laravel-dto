<?php

namespace Qqs\Dto\Tests\Unit\Mocks;

class SomeChild extends SomeParent
{
    public function getParentProperty()
    {
        return $this->someParentProperty;
    }
}