<?php

namespace Qqs\Dto\Tests\Unit\Mocks;

use Qqs\Dto\Dto;

class UntypedDto extends Dto
{
    public $name;
    public $password;
    public $email;
    public $age;
}
