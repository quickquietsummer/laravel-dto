<?php

namespace Qqs\Dto\Tests\Unit\Mocks;

use DateTime;
use Qqs\Dto\Dto;

class GoodDtoWithNarrowedType extends Dto
{
    public string $name;
    public string $password;
    public string $email;
    public DateTime $birthdate;
    public int $age;
    public SomeChild $parent;
    public ?string $favoriteColor;
}
