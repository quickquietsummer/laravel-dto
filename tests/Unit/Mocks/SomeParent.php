<?php

namespace Qqs\Dto\Tests\Unit\Mocks;

class SomeParent
{
    public function __construct(public string $someParentProperty)
    {
    }
}