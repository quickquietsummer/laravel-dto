<?php

namespace Qqs\Dto\Tests\Unit\Mocks;

use DateTime;
use Qqs\Dto\Dto;

class GoodDto extends Dto
{
    public string $name;
    public string $password;
    public string $email;
    public DateTime $birthdate;
    public int $age;
    public SomeParent $parent;
    public ?string $favoriteColor;
}
