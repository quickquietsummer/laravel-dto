<?php

namespace Qqs\Dto\Providers;

use Illuminate\Support\ServiceProvider;
use Qqs\Dto\Commands\MakeCommand;

class ConsoleServiceProvider extends ServiceProvider
{

    public function boot()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                MakeCommand::class
            ]);
        }
    }

}