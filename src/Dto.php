<?php

namespace Qqs\Dto;

use InvalidArgumentException;
use Reflection;
use ReflectionClass;
use ReflectionObject;
use ReflectionProperty;

class Dto
{
    /**
     * @var array<ReflectionProperty>
     */
    private array $properties;

    public function __construct(array $args)
    {
        $this->reflectProperties();
        $this->setProperties($args);
    }

    private function setProperties(array $args): void
    {
        foreach ($this->properties as $property) {
            $name = $property->name;
            $type = $property->getType();

            $isRequiredParameterMissing = empty($args[$name]);
            if ($isRequiredParameterMissing) {
                if ($type->allowsNull()) {
                    continue;
                }

                $this->throwRequiredParameterMissing($name);
            }

            $arg = $args[$name];
            $typeNameInDto = $type->getName();
            $typeNameInArg = get_debug_type($arg);

            if (is_object($arg)) {
                $isTypeMismatched = $arg instanceof $typeNameInDto === false;
            } else {
                $isTypeMismatched = $typeNameInDto !== $typeNameInArg;
            }
            if ($isTypeMismatched) {
                $this->throwTypeMismatched($name, $typeNameInDto, $typeNameInArg);
            }

            $this->{$name} = $arg;
        }
    }

    private function reflectProperties(): void
    {
        $reflection = new ReflectionClass(static::class);
        $properties = $reflection->getProperties(ReflectionProperty::IS_PUBLIC);

        foreach ($properties as $property) {
            if ($property->hasType() === false) {
                $this->throwTypeMissing($property->name);
            }

            $this->properties[] = $property;
        }
    }

    private function throwTypeMissing(string $propertyName): void
    {
        throw new \RuntimeException("Отсутствует тип для свойства $propertyName.
        " . static::class);
    }

    private function throwTypeMismatched(string $propertyName, string $typeInDto, string $typeInArg): void
    {
        throw new InvalidArgumentException("Несовпадение типов $propertyName при создании ДТО.
                В ДТО => $typeInDto
                В переданных параметрах => $typeInArg
                " . static::class);
    }


    public function throwRequiredParameterMissing(string $name): void
    {
        throw new InvalidArgumentException("Обязательное свойство ДТО $name отсутствует.
        " . static::class);
    }
}
